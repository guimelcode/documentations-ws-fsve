
[TOC]


# Le blog ou comment ça marche ? — The blog or how it works ?

## Bienvenue — Welcome

:flag_fr:

Nous mettons en place pour ce workshop un blog qui compilera des billets reprennant les signes/messages initités lors du-dit workshop.
Le choix de la plate-forme de blog s'est fait sur sa capacité de personnalisation, de prise en main, et de gestion du sytème blog.
Nous nous sommes tourner vers Grav qui est un outils CMS opensource assez flexible et fonctionnement avec une base de donnée plate (en fichiers plats) :smile:, c'est à dire facilement transportable et interprétable. Pourquoi cette dernière fonction ? Et bien pour pouvoir plus facilement manipuler le contenu compilé pour les restitutions futures.

Vous serez invité à vous connecté à la plateforme par mail.
Ce document vous permettra de vous familiariser avec le back-office de Grav.
Un autre document reprendra la méthodologie pour la personnalisation du site en soi.


:flag_gb:

We are setting up a blog for this workshop which will compile posts containing the signs / messages initiated during the said workshop.
The choice of blog platform was made on its capacity for customization , handling, and management of the blog system.
We turned to Grav which is a fairly flexible open source CMS tool and works with a flat database (in flat files) :smile:, ie easily transportable and interpretable. Why this last function? Well to be able to more easily manipulate the content compiled for future restitutions.

You will be invited to connect to the platform by email.
This document will allow you to familiarize yourself with the Grav back-office.
Another document will take up the methodology for personalizing the site itself.

---

## Grav, qu'est-ce que c'est ?

:flag_fr: Venant de https://getgrav.org/about
>Les origines de **Grav** viennent d'un désir personnel de travailler avec une plate-forme open source qui se concentre sur **la vitesse** et **la simplicité**, plutôt que sur une abondance de fonctionnalités intégrées qui se font au détriment de la complexité.
>
> Il existe de nombreuses plates-formes CMS open source, incluant mes favorites que sont [Joomla](https://www.joomla.org/) et [WordPress](https://wordpress.org/), ainsi que certaines plates-formes prometteuses très prometteuses comme [PageKit](https://pagekit.com/).
>
> Toutes ces plates-formes comptant sur une base de données pour la persistence des données, sont puissantes et offrent un bon degré de flexibilité.
>
>Un véritable inconvénient de ces plateformes est qu'elles nécessitent un réel engagement pour apprendre à les utiliser et à les développer. Vous devez vraiment en choisir un dans le pack et vous consacrer à cette plate-forme si vous souhaitez devenir compétent en tant qu'utilisateur, développeur ou administrateur.
>
>Et s'il y avait une plate-forme **rapide**, **facile à apprendre**, mais toujours **puissante et flexible**? Il existe sûrement déjà quelque chose qui répond à ces critères? Dans ma recherche d'une telle plate-forme, il est devenu clair qu'un CMS basé sur des **fichiers plats** était probablement la réponse, et il y a beaucoup de choix! J'ai créé une liste d'exigences que je pensais assurer une plateforme idéale pour mes besoins:
>  - Rapide, prêt à l'emploie
>  - Basé sur des fichiers plats
>  - Contenu créer en [Markdown](https://en.wikipedia.org/wiki/Markdown)
>  - Templating fournis par [Twig](https://twig.symfony.com/) ou un projet déjà établis
>  - Extensible et flexible via une architecture de plugin de grande envergure
>  - Simple d'installation et avec un minimum de spécifications serveur
>  - Devant être open source et en licence MIT si poossible
>  - Documentation solide
>  - Agréable à utiliser
>
>Le problème était que rien ne correspondait vraiment à mes besoins. Celles qui répondaient le mieux à mes exigences n'étaient pas open source, donc l'option de la forger et d'y ajouter les fonctionnalités que je voulais n'était pas disponible. Il me restait deux options:
>
>1. Commencer avec l'une des plateformes open source et la transformer en ma solution idéale.
>1. Commencer à partir de zéro.
>
>À l'origine, je pensais que [Pico](http://picocms.org/) pourrait constituer une bonne base de départ car il satisfaisait déjà une bonne partie des exigences. Cependant, en approfondissant, j'ai réalisé que cela n'allait pas faire un excellent point de départ en raison de son approche fonctionnelle. Donc, l'option # 2 était la seule option qui me restait. J'ai recommencé.
>
>**Grav** est fortement inspiré par toute une série d'autres plates-formes, mais est écrit à partir de zéro en se concentrant sur **la vitesse**, **la simplicité** et **la flexibilité**.
>
>Le cœur de Grav est construit autour du concept de dossiers et de fichiers Markdown pour le contenu. Ces dossiers et fichiers sont automatiquement compilés en HTML et mis en cache pour les performances.
>
>Ses pages sont accessibles via des URL qui se rapportent directement à la structure de dossiers qui sous-tend l'ensemble du CMS. En rendant les pages avec des [modèles Twig](https://twig.symfony.com/), vous avez un contrôle complet sur l'apparence de votre site, sans pratiquement aucune limitation.
>
>Une partie de la flexibilité provient de la fonctionnalité de **taxonomie** simple mais puissante de Grav qui vous permet de créer des relations entre les pages. Un autre élément clé de cette flexibilité est **l'architecture de plugin** qui existe sur toute la plate-forme pour vous permettre d'interagir et de modifier à peu près n'importe quelle partie de Grav selon les besoins.
>
>Veuillez [lire notre documentation](https://learn.getgrav.org/16) pour mieux comprendre comment fonctionne Grav et comment il peut vraiment changer vos idées sur le développement Web.
>
>[Andy Miller](https://twitter.com/rhuk) - Grav Lead Developer

:flag_gb: From https://getgrav.org/about

>The origins of **Grav** come from a personal desire to work with an open source platform that focused on **speed** and **simplicity**, rather than an abundance of built-in features that come at the expense of complexity.
>
>There are plenty of great open source CMS platforms out there, including personal favorites [Joomla](https://www.joomla.org/) and [WordPress](https://wordpress.org/), as well as some really promising up-and-coming platforms like [PageKit](https://pagekit.com/).
>
>All of these platforms rely on a database for data persistence, are powerful, and offer a good degree of flexibility.
>
>One real downside to these platforms is they require a real commitment to learn how to use and develop on them. You really have to pick one out of the pack, and dedicate yourself to that platform if you wish to become competent as either a user, developer, or administrator.
>
>What if there was a platform that was **fast**, **easy-to-learn**, and still **powerful & flexible**? Surely something already exists that meets these criteria? In my search for such a platform, it became clear that a flat-file based CMS was likely to be the answer, and there are a bunch to choose from! I created a list of requirements I thought would ensure an ideal platform for my needs:
>  - Fast, right out of the box
>  - Flat-file based
>  - Content created in [Markdown](https://en.wikipedia.org/wiki/Markdown)
>  - Templating provided by [Twig](https://twig.symfony.com/) or a similar established project
>  - Extensible and flexible via far-reaching plugin architecture
>  - Simple to install, with minimal server requirements
>  - Must be open source and MIT licensed if possible
>  - Solid Documentation
>  - Enjoyable to use
>
>The problem was, nothing really fit my requirements exactly. The ones that met my requirements the closest were not open source, so the option of forking it and adding the features I wanted was not available. I was left with two options:
>
>1. Start with one of the open source platforms and transform it into my ideal solution.
>1. Start from scratch
>
>Originally, I thought that [Pico](http://picocms.org/) might make a good base to start from as it already satisfied a good deal of the requirements. However, as I delved deeper, I realized it was not going to make a great starting point due to its functional approach. So, option #2 was the only option left to me. I started over.
>
>**Grav** is heavily inspired by a whole raft of other platforms, but is written from scratch focusing on **speed**, **simplicity**, and **flexibility**.
>
>The core of Grav is built around the concept of folders and markdown files for content. These folders and files are automatically compiled into HTML and cached for performance.
>
>Its pages are accessible via URLs that directly relate to the folder structure that underpins the whole CMS. By rendering the pages with [Twig Templates](https://twig.symfony.com/), you have complete control over how your site looks, with virtually no limitations.
>
>Part of the flexibility comes from Grav's simple, but powerful, **taxonomy** functionality that lets you create relationships between pages. Another key part of this flexibility is the **plugin architecture** that exists throughout the entire platform to allow you to interact and modify pretty much any part of Grav as needed.
>
>Please [read through our documentation](https://learn.getgrav.org/16) to get a better understanding of how Grav works, and how it can really change your ideas about web development.
>
>[Andy Miller](https://twitter.com/rhuk) - Grav Lead Developer

---

## Connexion et éditer son profil

:flag_fr:

Tout d'abord, vous allez recevoir un mail contenant votre identifiant ainsi que votre mot de passe.
Vous pourrez vous rendre sur https://blog.marionbonjour.net/admin

:flag_gb:

First, you will receive an email containing your username and password.
You can go to https://blog.marionbonjour.net/admin

> :flag_fr: — Le blog se trouve sur l'hébergement de Marion Bonjour dans ce sous-domaine "blog" en attendant un nom de domaine plus approprié.
>
> :flag_gb: — The blog is located on Marion Bonjour's hosting in this "blog" sub-domain while waiting for a more appropriate domain name.

![Grav Login](./img/grav-login.png "Page de login — Login Page")*Page de login — Login Page*

**:flag_fr: Vous pouvez alors vous connecter ! Vous vous trouvez sur votre dashboard.**

**:flag_gb: You can then connect! You are on your dashboard.**

![Grav Login](./img/grav-dashboard.png "Page de tableau de bord — Dashboard Page")*Page de tableau de bord — Dashboard Page*

:flag_fr:

Avant d'aller plus loin, il est toutefois très conseillé d'aller modifier votre mot de passe en cliquant sur l'icône à gauche, en dessous du logo GRAV (ou l'url https://blog.marionbonjour.net/admin/user/"votre nom d'utilisateur").
Vous pourrez d'ailleur personnaliser d'autres aspects de votre profil.

:flag_gb:

Before going further, it is however very advisable to change your password by clicking on the icon on the left, below the GRAV logo (or the url https://blog.marionbonjour.net/admin/user/"your_username").
You can also customize other aspects of your profile.

![Grav Login](./img/grav-profil.png "Page de profil — User Page")*Page de profil — User Page*

---

## Créer un billet de blog — Create a blog post

_:flag_fr: Bon, on est là pour la publication de billets ! Allons-y !_
_:flag_gb: Well, we're here for the publication of posts! Let's go!_


### L'interface de Pages — Pages interface

:flag_fr:

En allant sur l'onglet Pages dans le menu verticale de gauche, vous accèderez à la structure des pages du site et pouvez parcourir l'ensemble des pages et billets.

:flag_gb:

By going to the Pages tab in the vertical menu on the left, you will access the page structure of the site and can browse all the pages and posts.

![Grav Login](./img/grav-page.png "Pages")*Pages*

:flag_fr:

La structure n'est à modifier en aucuns cas, sans affecter le bon fonctionnement du site.
Les billets de blog sont stockés dans la page/dossier Blog, qui est également la page d'accueil du site (indiqué par l'icône maison :house:). Vous pouvez dérouler le dossier Blog en cliquant sur l'icône plus, dévoilant les billets enregistrés.

:flag_gb:

The structure should not be changed in any case, without affecting the proper functioning of the site.
Blog posts are stored in the Blog page / folder, which is also the site's home page (indicated by the house icon :house:). You can expand the Blog folder by clicking on the plus icon, revealing the saved posts.

:flag_fr: Vous touverez un légende des couleurs utilisées en bas de page.

:flag_gb: You will find a legend of the colors used at the bottom of the page.

![Grav Login](./img/grav-page-zoom.png "Légende — Legend")*Légende — Legend*

### Ajouter un billet — Add a post

:flag_fr:

Pour ajouter un billet, il faut en premier lieu cliquer sur le bouton Add dans le menu d'entête.

:flag_gb:

To add a post, first click on the Add button in the header menu.

![Grav Login](./img/grav-add-post.png "Bouton Add — post/page — Add button")*Bouton Add — post/page — Add button*

:flag_fr:

Ceci ouvre une boîte de dialogue Add Page. Plusieurs champs sont à renseigner pour bien ranger le billet :
+ Page Title : Le nom de l'article
+ Folder Name : Se remplira tout seul, c'est le nom du fichier/dossier qui va être créer
+ Parent Page : par défaut sur `/(root)`, il faudra le changer pour `--> (blog) Blog`
+ Page Template : par défaut sur `Default`, à changer pour `Item`
+ Visible : laisser Auto

:flag_gb:

This opens an Add Page dialog box. Several fields must be completed to properly store the post :
+ Page Title : name of the posts
+ Folder Name : Will fill itself, it's the name of the file / folder that will be created
+ Parent Page : defaults to `/(root)`, it will have to be changed to `--> (blog) Blog`
+ Page Template : defaults to `Default`, it will have to be changed to `Item`
+ Visible : let Auto

![Grav Login](./img/grav-add-post-dialog.png "Boite de dialogue — Dialog box")*Boite de dialogue — Dialog box*

:flag_fr: Cliquer sur Continue pour passer à la suite.

:flag_gb: Click on Continue.

---

## Écriture d'un billet et ajout de média — Write a post and add a media

:flag_fr:

On arrive enfin sur une page d'édition de contenu !
Rien de bien exceptionnel pour du html : des champs et zones de textes.

:flag_gb:

We finally arrive on a content editing page!
Nothing very exceptional for html : fields and text areas.

![Grav Login](./img/grav-post.png "Page d'édition de billet — Post edition page")*Page d'édition de billet — Post edition page*

:flag_fr: Partons pour une description des différents éléments de l'interface.

:flag_gb: Let's start for a description of the different elements of the interface.

### Rédiger son billet — Write your post
#### Le titre — Title

:flag_fr:

Le champs *Title* a été rempli à l'étape précédente et apparait ici.
Il peut être modifié ; cela n'aura pas d'effet sur le chemin du billet.

:flag_gb:

The *Title* field was filled in in the previous step and appears here.
It can be changed; this will have no effect on the post path.

#### Le corps du billet — Post body

:flag_fr:

Vous pouvez composer ici votre contenu en vous aidant de la barre d'actions.
Le contenu est formaté en Markdown (voir le [document explicatif](./markdown-document.md)). Les actions de mise en forme de la barre d'actions ajoutent les marqueurs associés.

:flag_gb:

You can compose your content here using the action bar.
Content is formatted in Markdown (see the [explanatory document](./markdown-document.md)). The formatting actions in the action bar add the associated markers.

##### Les images — Images

:flag_fr:

On ne peut ajouter des images que lorsque le billets a été sauvegarder. Ceci fait apparaître un champ de dépôt en-dessous de la zone de texte.

:flag_gb:

You can only add images when the posts has been saved. This brings up a drop field below the text box.

![Grav Login](./img/grav-post-img-no-area.png "Note de bas de page indiquant de sauver le billet pour ajouter des images — Footnote indicates to save the post to add images")*Note de bas de page indiquant de sauver le billet pour ajouter des images — Footnote indicates to save the post to add images*

![Grav Login](./img/grav-post-image-area.png "Champ de dépôt des images — Images drop field")*Champ de dépôt des images — Images drop field*

:flag_fr:

Il y a quelques prérequis pour que l'image soit facilement importable sur le blog :
- Le DPI doit être à 72, correspondant à l'affichage sur la plupart des écrans.
- La hauteur et la largeur ne doivent pas être trop grande au risque d'occuper trop d'espace.
On pourra toute fois résoudre ce soucis par des commande en ligne texte pour les plus téméraires.
- Le fichier image doit avoir un nom simple, sans accents, ni espaces, ou autres caractères spéciaux car ceux-ci seront remplacés par des symboles compliquant l'appel du ficher.

:flag_gb:

There are some prerequisites for the image to be easily importable on the blog:
- The DPI should be 72, corresponding to the display on most screens.
- The height and the width should not be too large, at the risk of occupying too much space.
We can however resolve this concern by ordering text online for the more adventurous.
- The image file must have a simple name, without accents, spaces, or other special characters because these will be replaced by symbols complicating the calling of the file.

**:flag_fr: Il est conseillé donc de faire des copies spécifique à la publication web.**

**:flag_gb: It is therefore advisable to make copies specific to the web publication.**

###### Importation

:flag_fr:

Cliquer sur le champ image (qui ouvrira une boite de dialogue de navigateur de fichier) ou glisser un ficher dans ce champ.
L'image va se chargé dans le site et fera partie de votre stock de média.

:flag_gb:

Click on the image field (which will open a file browser dialog) or drag a file into this field.
The image will load into the site and will be part of your media inventory.

![Grav Login](./img/grav-post-image-area-items.png "Images ajoutées — Added images")*Images ajoutées — Added images*

###### Ajout au corps de billet — Add to the post's body

:flag_fr: Pour ajouter une images au corps du billet, cliquer sur l'icône du menu "image" ce qui va ajouter ce code :
:flag_gb: To add an image to the body of the post, click on the menu icon "image" which will add this code:
```
![](http://)
```
:flag_fr: La structure de ce code est la suivante :
:flag_gb: The structure of this code is as follows:
```
![texte_alternatif](lien_du_média)
![alternative_text](media_link)
```

:flag_fr:

Le texte alternatif permet de substituer l'image si elle ne se charge pas et aussi à son référencement.
Le lien du média peut être un lien externe (démarrant par `http://` ou `https://`) ou un lien interne renvoyant à un fichier chargé.

Exemple :
```
![texte_alternatif](nom_de_mon_image.jpg)
```

:flag_gb:

The alternative text makes it possible to substitute the image if it does not load and also for its referencing.
The media link can be an external link (started with `http://` or `https://`) or an internal link referring to a loaded file.

Example :
```
![alternative_text](name_of_the_file.jpg)
```

:flag_fr:

On peut l'améliorer comme suit pour ajouter une description
```
![texte_alternatif](lien_du_média "texte de description")
```
Ceci permettra d'afficher un texte en dessous de l'image.
> Ne marche pas tout à faire pour le moment.

:flag_gb:

It can be improved as follows to add a description
```
![alternative_text](media_link "Description text")
```
This will display text below the image.
> Not doing everything yet.

##### Les vidéos — Videos

:flag_fr:

Pour les vidéos, il va falloir héberger les vidéos sur un service dédié, comme Vimeo ou Youtube, est intergré le lien de la vidéo dans le `lien_du_media` du code d'image. Tout le paramètrage est identique.

:flag_gb:

For videos, we will have to host the videos on a dedicated service, such as Vimeo or Youtube, the video link is integrated in the `media_link` of the image code. All the settings are identical.
