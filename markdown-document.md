---
puppeteer:
  displayHeaderFooter: true
  footerTemplate: "
  <style>
  footer {
      font-family: 'IBM Plex Mono', monospace;
      font-size: 8pt;
      color: black;
      text-align: right;
      display: block;
      width: 95%;
      padding: 2rem;

    }
  </style>
  <footer>
  <span class='pageNumber'></span> /
  <span class='totalPages'></span></footer>
  "
---

[TOC]

<!-- pagebreak -->

# Petit document pour se familiariser avec le Markdown



## Qu'est-ce que c'est ce truc ? (source: [Wikipedia](https://fr.wikipedia.org/wiki/Markdown))

![Markdown logo](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg?uselang=fr)

**Markdown** est un [langage de balisage léger](https://fr.wikipedia.org/wiki/Langage_de_balisage_l%C3%A9ger) créé en 2004 par [John Gruber](https://fr.wikipedia.org/wiki/John_Gruber) avec l'aide d'[Aaron Swartz](https://fr.wikipedia.org/wiki/Aaron_Swartz) [^1][^2]. Son but est d'offrir une [syntaxe](https://fr.wikipedia.org/wiki/Syntaxe) facile à lire et à écrire. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières.

Un document balisé par Markdown peut être converti en [HTML](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language), en [PDF](https://fr.wikipedia.org/wiki/Portable_Document_Format) ou en d'autres formats. Bien que la syntaxe Markdown ait été influencée par plusieurs filtres de conversion de texte existants vers HTML — dont [Setext](https://fr.wikipedia.org/wiki/Setext)[^3], atx[^4], [Textile](https://fr.wikipedia.org/wiki/Textile_(langage)), [reStructuredText](https://fr.wikipedia.org/wiki/ReStructuredText), Grutatext[^5] et EtText[^6] —, la source d’inspiration principale est le format du [courrier électronique](https://fr.wikipedia.org/wiki/Courrier_%C3%A9lectronique) en mode texte.

[^1]: ["Markdown"](http://www.aaronsw.com/weblog/001189)
[^2]: ["Markdown"](https://web.archive.org/web/20040402182332/http://daringfireball.net/projects/markdown/)
[^3]: [Setext](https://docutils.sourceforge.io/mirror/setext.html)
[^4]: [atx](http://www.aaronsw.com/2002/atx/)
[^5]: [Grutatext](https://triptico.com/software/grutatxt.html)
[^6]: [EtText](http://ettext.taint.org/doc/)

## Pourquoi utiliser ce langage ?
Et bien il y a plusieurs raisons :
1. Slack intègre un moteur de rendu Markdown pour la gestion de mise en forme des messages. On peut facilement mettre en gras, en italique, en citation sans passer par l'interface graphique.
2. Le blog mis en place pour le workshop intègre un moteur de rendu Markdown pour l'écriture des billets dee blog.

<!-- pagebreak -->

## Comment ça marche ?
Comme dit plus haut, c'est un langage de balisage comme peut l'être le HTML et le XML. Mais les "balises" ne se manifestent pas des mots encadrés par des chevrons ("<" et ">") encadrant le contenu à organiser.
À la place, nous avons un ensemble de signes et combinaison de signes permettant le formatage du contenu.

__Voyons tout ça plus en détail et en pratique.__
_Nous allons reprendre ici le [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) répondant à la mise en forme de GitHub_

<!-- pagebreak -->

### En-têtes
```
# H1
## H2
### H3
#### H4
##### H5
###### H6

Pour les H1 et H2, on peut utiliser une stylisation rappelant un soulignement

Alt-H1
======

Alt-H2
------

```

# H1
## H2
### H3
#### H4
##### H5
###### H6

Pour les H1 et H2, on peut utiliser une stylisation rappelant un soulignement

Alt-H1
======

Alt-H2
------

---

<!-- pagebreak -->

### Emphases
```
L'emphase en typographie ou l'accentuation par le style de caractère, dont l'italique, ce fait avec des *astérisques* or des _underscores_.

Le gras s'obtient avec des doubles **asterisques** or doubles __underscores__.

On peut combiner les deux stylisation, **asterisques et _underscores_**.

On peut barreé avec deux tildes. ~~Rayonnons ça.~~

```

L'emphase en typographie ou l'accentuation par le style de caractère, dont l'italique, ce fait avec des *astérisques* or des _underscores_.

Le gras s'obtient avec des doubles **asterisques** or doubles __underscores__.

On peut combiner les deux stylisation, **asterisques et _underscores_**.

On peut barreé avec deux tildes. ~~Rayonnons ça.~~

---

<!-- pagebreak -->

### Listes
(Dans cet exemple, espaces ou tabulations sont symbolisées par le signe " · ")
```
1. Premier item d'une liste ordonée.
2. Un autre item.
···* Sous-liste non-ordonnée.
1. La valeur du nombre n'a pas d'important, il faut juste que ce soit un nombre.
···1. Sous-liste ordonée
4. Et un autre item.

···On peut aligner un texte à un élément de liste en respectant la même indentation du texte (le nombre d'espace avec le début de la ligne de contenu).

* Les listes non-ordonnée peuvent être déclarée par une astérisque.
- Ou par le signe moins
+ Ou par le signe plus
```
1. Premier item d'une liste ordonée.
2. Un autre item.
   * Sous-liste non-ordonnée.
1. La valeur du nombre n'a pas d'important, il faut juste que ce soit un nombre.
   1. Sous-liste ordonée
4. Et un autre item.

   On peut aligner un texte à un élément de liste en respectant la même indentation du texte (le nombre d'espace avec le début de la ligne de contenu).

* Les listes non-ordonnée peuvent être déclarée par une astérisque.
- Ou par le signe moins
+ Ou par le signe plus

---

<!-- pagebreak -->


### Liens
Il y a deux façons de créer des hyperliens.

```
[Je suis un lien (en une seule ligne)](https://www.google.com)

[Je suis un lien (en une seule ligne) avec un titre (apparaissant au survole du lien)](https://www.google.com "Google's Homepage")

[Je suis un lien (par référence)][Texte arbitraire de référence sensible à la casse]

[Je suis un lien relatif à un fichier d'un dépôt ou d'un dossier](../blob/master/LICENSE) -- Ceci ne marche pas dans notre contexte

[On peut aussi utiliser des nombres pour un lien de référence][1]

Ou laisser vide et utiliser le [texte comme lien].

Les URL et les URL entre chevrons vont automatiquement devenir des liens.
http://www.example.com ou <http://www.example.com> et parfois example.com peuvent fonctionner (pas sur Github).

Du texte pour montrer que les liens de référence peuvent suivre plus tard.

[Texte arbitraire de référence sensible à la casse]: https://www.mozilla.org
[1]: http://slashdot.org
[texte comme lien]: http://www.reddit.com
```

[Je suis un lien (en une seule ligne)](https://www.google.com)

[Je suis un lien (en une seule ligne) avec un titre (apparaissant au survole du lien)](https://www.google.com "Google's Homepage")

[Je suis un lien (par référence)][Texte arbitraire de référence sensible à la casse]

[Je suis un lien relatif à un fichier d'un dépôt ou d'un dossier](../blob/master/LICENSE) -- Ceci ne marche pas dans notre contexte

[On peut aussi utiliser des nombres pour un lien de référence][1]

Ou laisser vide et utiliser le [texte comme lien].

Les URL et les URL entre chevrons vont automatiquement devenir des liens.
http://www.example.com ou <http://www.example.com> et parfois example.com peuvent fonctionner (pas sur Github).

Du texte pour montrer que les liens de référence peuvent suivre plus tard.

[Texte arbitraire de référence sensible à la casse]: https://www.mozilla.org
[1]: http://slashdot.org
[texte comme lien]: http://www.reddit.com

Pour que la cible du lien s'ouvre dans un nouvel onglet, ajouter `?target=_blank` après l'url.
For the target of the link open in a new tab, add `?target=_blank` after url.

---

<!-- pagebreak -->

### Images

```
Voici un logo (survoler l'image pour voir le titre) :

En une seule ligne :
![texte alternatif](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Texte du ttre du logo 1")

Par référence:
![texte alternatif][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Texte du ttre du logo 2"

```

Voici un logo (survoler l'image pour voir le titre) :

En une seule ligne :
![texte alternatif](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Texte du ttre du logo 1")

Par référence:
![texte alternatif][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Texte du ttre du logo 2"

---

<!-- pagebreak -->

### Code et mise en évidence de la syntaxe (Syntax Highlighting)

Les blocs de code font partie de la spécification du Markdown, mais ce n'est pas le cas pour la mise à valeur de la syntaxe. Toutefois, beaucoup de moteurs de rendu -- comme celui de *Github* et de *Markdown Here* -- prennent en charge la mise en évidence de la syntaxe. Quels langages sont pris en charge ainsi que comment le nom de ces langages doit être écrit varie selon les moteurs. *Markdown Here* prends en charge des dizaines de langages (ainsi que des quasi-langages comme les commandes UNIX et les entêtes HTTP) ; pour voir la liste complète, et comment écrire le nom des langages, aller sur [la page demo de highlight.js](https://highlightjs.org/static/demo/).

```
Un `morceau de code trois` dans la ligne est entouré `d'accents graves`.
```
Un `morceau de code trois` dans la ligne est entouré `d'accents graves`.

Les blocs de code sont soit clôturés par des lignes avec trois accents graves ` ``` `, ou sont indentés pas quatre espaces. Il est recommendé d'utiliser seulement la méthode clôturée car elle met plus en évidence le contenu et permet la misee en valeur de la syntaxe.

```
      ```javascript
      var s = "Mise en évidence de la syntaxe du JavaScript";
      alert(s);
      ```

      ```python
      s = "Mise en évidence de la syntaxe du Python"
      print s
      ```

      ```
      Si aucun langage n'est indiqué, il n'y aura pas de syntaxe mise en évidence.
      ```
```

```javascript
var s = "Mise en évidence de la syntaxe du JavaScript";
alert(s);
```

```python
s = "Mise en évidence de la syntaxe du Python"
print s
```

```
Si aucun langage n'est indiqué, il n'y aura pas de syntaxe mise en évidence.
```

---

<!-- pagebreak -->


### Tableaux

```
Les deux-points peuvent être utilisés pour aligner le contenu des colonnes.

| Les tableaux  | Sont             | Cool      |
| ------------- |:----------------:| ---------:|
| col 3 est     | alignée à droite | 1600,00 € |
| col 2 est     | centrée          |   12,00 € |

Il doit y avoir au moins 3 tirets séparant chaque cellule d'en-tête.
Les barres verticales (|) extérieures sont optionnelles, et pas besoin de mettre joliment en forme les lignes.

Markdown | Moins | Jolie
--- | --- | ---
*Rends* | `encore` | **bien**
1 | 2 | 3

```
Les deux-points peuvent être utilisés pour aligner le contenu des colonnes.

| Les tableaux  | Sont             | Cool      |
| ------------- |:----------------:| ---------:|
| col 3 est     | alignée à droite | 1600,00 € |
| col 2 est     | centrée          |   12,00 € |

Il doit y avoir au moins 3 tirets séparant chaque cellule d'en-tête.
Les barres verticales (|) extérieures sont optionnelles, et pas besoin de mettre joliment en forme les lignes.

Markdown | Moins | Jolie
--- | --- | ---
*Rends* | `encore` | **bien**
1 | 2 | 3

---

<!-- pagebreak -->

### Blocs de citation

```
> Les blocs de citation sont très pratique pour émuler des textes de réponses (échange de Chat ou d'e-mail) ou mettre en évidence une citation.
> Cette ligne fait partie de la même citation.

Rupture de la citation.

> Il s'agit d'une très longue ligne qui sera toujours correctement en citation lorsqu'elle sera terminée. Aller, continuons à écrire pour être sûre qu'elle soit assez longue. On peut aussi *ajouter* des styles **Markdown** dans unee citation.

```

> Les blocs de citation sont très pratique pour émuler des textes de réponses (échange de Chat ou d'e-mail) ou mettre en évidence une citation.
> Cette ligne fait partie de la même citation.

Rupture de la citation.

> Il s'agit d'une très longue ligne qui sera toujours correctement en citation lorsqu'elle sera terminée. Aller, continuons à écrire pour être sûre qu'elle soit assez longue. On peut aussi *ajouter* des styles **Markdown** dans unee citation.

---

<!-- pagebreak -->


### Séparateurs horizontaux

```
Trois ou plus...

---

Traits-d'union

***

Astérisques

___

Underscores (tirets du bas)

```

Trois ou plus...

---

Traits-d'union

***

Astérisques

___

Underscores (tirets du bas)
